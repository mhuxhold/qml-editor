#ifndef EXPORT_H
#define EXPORT_H

#include <QObject>
#include <QTransform>

class Export : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString Author READ Author WRITE setAuthor NOTIFY AuthorChanged)
	Q_PROPERTY(QTransform Test READ Test WRITE setTest NOTIFY TestChanged)
public:
	Export();
	void		setAuthor(const QString &a_strAuthor);
	QString		Author();

	void		setTest(const QTransform &a_strString);
	QTransform		Test();
signals:
	void		AuthorChanged();
	void		TestChanged();
private:
	QString		m_strAuthror;
	QTransform	m_strTest;

};

#endif // EXPORT_H
