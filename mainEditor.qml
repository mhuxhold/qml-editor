import QtQuick 2.6
import QtQuick.Window 2.1
/*
5.5in (iPhone 6 Plus):
1242 × 2208px portrait

4.7in (iPhone 6):
750 × 1334px portrait

4in (iPhone 5 and 5s):
640 × 1096px portrait (without status bar) minimum
640 × 1136px portrait (full screen) maximum
1136 × 600px landscape (without status bar) minimum
1136 × 640px landscape (full screen) minimum

3.5in (iPhone 4 and 4s):
640 × 920px portrait (without status bar) minimum
640 × 960px portrait (full screen) maximum
960 × 600px landscape (without status bar) minimum
960 × 640px landscape (full screen) maximum
*/



Window {
	id: root
	visible: true
	width: Screen.width/2-200; height: Screen.height/2-200;  // this here sets up the ascpect ratio
	color: "black"
	property int highestZ: 0
	property real defaultSize: 200
	property var currentFrame: undefined
	property real surfaceViewportRatio: 1.5 // this here is going to affect the ratio

	//Main area that contains the image that we want to get the data from
	//Will have to iterate throught the children functions to get all the right locations out
	Flickable{
		id: flick
		anchors.fill: parent
		contentWidth: parent.width
		contentHeight: parent.height

		//so the ratio will have to be calculated and maintained on the creation of the object
		//If the frame is 400 x 300 then we need to create the ratio on the fly
		Rectangle{
			id: photoFrame
			width: imaged.width / 2// * (1 + 0.10 * image.height / image.width)
			height: imaged.height / 2// * 1.10
			border.color: "black"
			border.width: 2
			smooth: true
			antialiasing: true
			onXChanged: {
				console.log("my x" + x)
			}
			focus: true
			Keys.onPressed: {
				if (event.key === Qt.Key_A) {
					console.log('Key A was pressed');
					event.accepted = true;
				}
				if (event.key === Qt.Key_Plus) {
					console.log("hit plus")
					event.accepted = true;
				}
				if (event.key === Qt.Key_Minus) {
					console.log("hit the minus");
					event.accepted = true;
				}
			}

			//make the photo show up in a random location
			Component.onCompleted: {
				//x = Math.random() * root.width - width / 2
				//y = Math.random() * root.height - height / 2
				x = 0
				y = 0
				//rotation = Math.random() * 13 - 6
			}
			Image {
				id: imaged
				anchors.centerIn: parent
				fillMode: Image.PreserveAspectFit
				source: "resources/moon.png"
				antialiasing: true
				property string propname :"Image"
				scale: .5
			}
			MouseArea {
				id: dragArea
				hoverEnabled: true
				anchors.fill: parent
				drag.target: photoFrame
				scrollGestureEnabled: false  // 2-finger-flick gesture should pass through to the Flickable
				onPressed: {
					photoFrame.z = ++root.highestZ;
					parent.setFrameColor();
				}
				onEntered: parent.setFrameColor();
				//detect the key presses and make the image rotate and scale and zoom
				onWheel: {
					if (wheel.modifiers & Qt.ControlModifier) {
						photoFrame.rotation += wheel.angleDelta.y / 120 * 5;
						if (Math.abs(photoFrame.rotation) < 4)
							photoFrame.rotation = 0;
					} else {
						photoFrame.rotation += wheel.angleDelta.x / 120;
						if (Math.abs(photoFrame.rotation) < 0.6)
							photoFrame.rotation = 0;
						var scaleBefore = photoFrame.scale;
						photoFrame.scale += photoFrame.scale * wheel.angleDelta.y / 120 / 10;
					}
				}
			}
			function setFrameColor() {
				if (currentFrame)
					currentFrame.border.color = "black";
				currentFrame = photoFrame;
				currentFrame.border.color = "red";
			}
		}
	}

	//button that will call the export.js and start the export process of the image
	Rectangle {
		id: button
		x:5
		y:5
		width: buttonText.width + 20
		height: 50
		color: "white"
		Text {
			id: buttonText
			font.pointSize: 12
			color: "black"
			text: "Export"
		}
		MouseArea{
			id: buttonMouseArea
			anchors.fill: parent
			onPressed: { //this is the jaca function call
				parent.startExport();
			}
		}
		function startExport() {

			console.log(Screen.width,Screen.height);

			console.log("Global cordinates: ");
			var pos_abs = flick.mapFromItem (flick.parent, photoFrame.x, photoFrame.y);
			console.log(pos_abs);


			//need to get the position of the image on the screen
			for ( var i = 0; i < flick.contentItem.children.length; i++) {
				console.log("outer loop");
				console.log("this is the point from the orgin of this object");
				console.log(flick.contentItem.children[i].x);
				console.log(flick.contentItem.children[i].y);

				//need to get the orgin and bring that over with us. Or do math from orgin of
				//flick area and be able to just hand a x,y,z cord over to make it simplier
				//this method could mess wiht the child's cord system and Matrix4x4


				var temp = flick.contentItem.children[i];
				//loop through its children
				for ( var j = 0; j < temp.children.length; j++ ) {
					console.log("inner loop");
					console.log(temp.children[j].x);
					console.log(temp.children[j].y);
				}
				console.log(temp);
			}
		}
	}
}
